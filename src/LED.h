#ifndef LED_H_
#define LED_H_

#include "stm32f0xx.h"

#define LED_RESOLUTION 1024
void LED_init();
void LED_set(uint16_t red, uint16_t green, uint16_t blue);
#endif
