#include "sampling.h"
#include "macros.h"
#include <stddef.h>
#include <stdbool.h>
#include <string.h>


const SAMPLING_analogChannel_t channels[] =
{
    [Channel_Microphone] = {.port = GPIOA, .pin = 0, .rccMask = RCC_AHBENR_GPIOAEN, .adcChannel = 0},
    [Channel_Red]        = {.port = GPIOA, .pin = 1, .rccMask = RCC_AHBENR_GPIOAEN, .adcChannel = 1},
    [Channel_Green]      = {.port = GPIOA, .pin = 2, .rccMask = RCC_AHBENR_GPIOAEN, .adcChannel = 2},
    [Channel_Blue]       = {.port = GPIOA, .pin = 3, .rccMask = RCC_AHBENR_GPIOAEN, .adcChannel = 3}
};

static uint8_t channelToBufferMapping[NELEMS(channels)] = {0xFF};
static volatile bool samplesReadyFlag = 0;

static void prepareEnumsToBufferMapping(void);

static void DMA1_Channel1_IRQHandler(void)
{
	if(DMA1->ISR & DMA_ISR_TCIF1)
	{
		DMA1->IFCR |= DMA_IFCR_CTCIF1;
		samplesReadyFlag = true;
	}
}

static bool addToArrayAscending(uint8_t num, uint8_t *array, size_t size)
{
    for(size_t i = 0; i < size; i++)
    {
        if(num < array[i])
        {
            if(i < size - 1)
            {
                memmove(&array[i + 1], &array[i], size - i - 1);
            }
            array[i] = num;
            return true;
        }
    }
    return false;
}

SAMPLING_measurement_t *SAMPLING_getMeasurement(void)
{
    uint16_t buffer[NELEMS(channels)];

	DMA1_Channel1->CCR &= ~DMA_CCR_EN;
	DMA1_Channel1->CCR |= DMA_CCR_MINC
						| DMA_CCR_MSIZE_0
					    | DMA_CCR_PSIZE_0
					   	| DMA_CCR_PL;
	DMA1_Channel1->CNDTR = NELEMS(buffer);
	DMA1_Channel1->CMAR = (uint32_t)buffer;
	DMA1_Channel1->CPAR = (uint32_t)&ADC1->DR;
	DMA1_Channel1->CCR |= DMA_CCR_EN;

	ADC1->ISR |= ADC_ISR_OVR;
	ADC1->CR |= ADC_CR_ADSTART;

    while(!(ADC1->ISR & ADC_ISR_EOSEQ));

    static SAMPLING_measurement_t measurement;
    //pilnowac kolejnosci kanalow!!
    for(size_t i = 0; i < NELEMS(channels); i++)
    {
        measurement.data[i] = buffer[channelToBufferMapping[i]];
    }

    return &measurement;
}

void SAMPLING_init(void)
{
	RCC->AHBENR |= RCC_AHBENR_DMA1EN;
	RCC->APB2ENR |= RCC_APB2ENR_ADC1EN | RCC_APB2ENR_TIM1EN;
    
    
    for(size_t i = 0; i < NELEMS(channels); i++)
    {
        SAMPLING_analogChannel_t *ch = &channels[i];
        RCC->AHBENR |= ch->rccMask;
        ch->port->MODER |= (3 << (ch->pin * 2));
        ch->port->PUPDR &= ~(3 << (ch->pin * 2));
    }

    ADC1->CR |= ADC_CR_ADCAL;
    while(ADC1->CR & ADC_CR_ADCAL);

	ADC1->CR |= ADC_CR_ADEN;
	while(!(ADC1->ISR & ADC_ISR_ADRDY));

	ADC1->CFGR1 |= ADC_CFGR1_DMAEN
				   | ADC_CFGR1_RES_0; //10-bit resolution
	ADC1->CFGR2 |= ADC_CFGR2_CKMODE_1;

    for(size_t i = 0; i < NELEMS(channels); i++)
    {
        SAMPLING_analogChannel_t *ch = &channels[i];
        ADC1->CHSELR |= (1 << ch->adcChannel);
    }
    prepareEnumsToBufferMapping();
}

static void prepareEnumsToBufferMapping(void)
{
    for(size_t i = 0; i < NELEMS(channels); i++)
    {
        addToArrayAscending(channels[i].adcChannel, channelToBufferMapping, 
                            NELEMS(channelToBufferMapping));
    }
}

