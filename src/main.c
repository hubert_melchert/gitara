#include "stm32f0xx.h"
#include "macros.h"
#include "sampling.h"
#include "LED.h"
#include <string.h>
#include <stdio.h>

static volatile uint32_t delay_ms = 0;

void SysTick_Handler(void)
{
    if(delay_ms)
    {
        delay_ms--;
    }
}

static void delay(uint32_t ms)
{
    delay_ms = ms;
    while(delay_ms);
}

int main(void)
{
    SysTick_Config(SystemCoreClock / 1000);
    NVIC_EnableIRQ(SysTick_IRQn);
    LED_init();
    LED_set(LED_RESOLUTION, 0, 0);
    delay(1000);
    LED_set(0, LED_RESOLUTION, 0);
    delay(1000);
    LED_set(0, 0, LED_RESOLUTION);
    delay(1000);
    LED_set(0, 0, 0);
    
    SAMPLING_init();
   

    while(1)
    {
        SAMPLING_measurement_t *ms = SAMPLING_getMeasurement();
        LED_set(ms->data[Channel_Red], ms->data[Channel_Green], ms->data[Channel_Blue]);
    }
}
