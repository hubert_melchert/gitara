#ifndef SAMPLING_H_
#define SAMPLING_H_
#include "stm32f0xx.h"
#include <stdbool.h>


enum
{
    Channel_Microphone = 0,
    Channel_Red,
    Channel_Green,
    Channel_Blue,
    Channel_MAX
};

typedef const struct
{
    GPIO_TypeDef *port;
    uint8_t pin;
    uint32_t rccMask;
    uint8_t adcChannel;
}SAMPLING_analogChannel_t;



typedef struct
{
    uint16_t data[Channel_MAX];
}SAMPLING_measurement_t;

void SAMPLING_init(void);
SAMPLING_measurement_t *SAMPLING_getMeasurement(void);

#endif /* SAMPLING_H_ */
