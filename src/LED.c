#include "LED.h"
#include "macros.h"
#include <stddef.h>


enum
{
    Led_Red = 0,
    Led_Green,
    Led_Blue
};

typedef struct
{
    GPIO_TypeDef *port;
    uint8_t pin;
    uint32_t rccMask;
    volatile uint32_t *pwmChannel;
}led_t;

static led_t leds[] = {
    [Led_Red] = {.port = GPIOB, .pin = 0, .rccMask = RCC_AHBENR_GPIOBEN, .pwmChannel = &(TIM3->CCR3)},
    [Led_Green] = {.port = GPIOA, .pin = 7, .rccMask = RCC_AHBENR_GPIOAEN, .pwmChannel = &(TIM3->CCR2)},
    [Led_Blue] = {.port = GPIOA, .pin = 6, .rccMask = RCC_AHBENR_GPIOAEN, .pwmChannel = &(TIM3->CCR1)},
};

void LED_init()
{
    for(size_t i = 0; i < NELEMS(leds); i++)
    {
        led_t *led = &leds[i];
        RCC->AHBENR |= led->rccMask;
        led->port->MODER &= ~(3 << (led->pin * 2));
        led->port->MODER |= (2 << (led->pin * 2)); //alternative mode
        if(led->pin < 8)
        {
            led->port->AFR[0] |= (1 << (led->pin * 4));
        }
        else
        {
            led->port->AFR[1] |= (1 << ((led->pin - 8) * 4));
        }
    }

    RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;
    TIM3->ARR = LED_RESOLUTION - 1;
	TIM3->PSC = 10;

	TIM3->CR1 |= TIM_CR1_ARPE;
    
    //PWM channel 1
	TIM3->CCMR1 |= TIM_CCMR1_OC1M_1 | TIM_CCMR1_OC1M_2 | TIM_CCMR1_OC1PE;
    TIM3->CCER |= TIM_CCER_CC1E; 

    //PWM channel 2
    TIM3->CCMR1 |= TIM_CCMR1_OC2M_1 | TIM_CCMR1_OC2M_2 | TIM_CCMR1_OC2PE;
    TIM3->CCER |= TIM_CCER_CC2E;

    //PWM channel 3
    TIM3->CCMR2 |= TIM_CCMR2_OC3M_1 | TIM_CCMR2_OC3M_2 | TIM_CCMR2_OC3PE;
    TIM3->CCER |= TIM_CCER_CC3E;
	TIM3->EGR |= TIM_EGR_UG;


	TIM3->CR1 |= TIM_CR1_CEN;
}

void LED_set(uint16_t red, uint16_t green, uint16_t blue)
{

    *(leds[Led_Red].pwmChannel) = red < LED_RESOLUTION ? red : LED_RESOLUTION;
    *(leds[Led_Green].pwmChannel) = green < LED_RESOLUTION ? green : LED_RESOLUTION;;
    *(leds[Led_Blue].pwmChannel) = blue < LED_RESOLUTION ? blue : LED_RESOLUTION;;
}
